# Python evolutionary password generator

This is an evolutionary program that generates human remeberable passwords,
by concatenating multiple words from a dictionary.

It's written in python and it is self documented.
It's meant to be executed in a shell.

It requires the words_alpha.txt dictionary by default ( avaible here
https://github.com/dwyl/english-words/), though you can specify
your own dictionary, if you wish.



here's the output of python password_generator -h :

*	-w WORDS, --words WORDS
                        Sets the maximum number of words of the password
*  -l LENGTH, --length LENGTH
                        Sets the maximum length of the password
*  -p POPULATION, --population POPULATION
                        Sets the population size
*  -g GENERATIONS, --generations GENERATIONS
                        Sets the number of generations. Feel free to trigger
                        the keyboard interrupt during the execution if g is
                        too high or the program slows down
*  -s SEED, --seed SEED  Sets the random seed. Useful for debugging or
                        thinkering around.
*  --separator SEPARATOR
                        Sets the word separator in the final output
*  --saveseparator SAVESEPARATOR
                        Set to True if you want to save the passwords with
                        their separators
*  --selection SELECTION
                        Sets the selection percentage between one generations.
                        Must be set between (0,1)
*  -f FINAL, --final FINAL
                        Sets the final selection percentage. Regulates the
                        number of output Passwords. Must be set between (0,1)
*  -S SAVE, --save SAVE  Sets the save flag. set to True if you want to save
                        your passwords
*  -n NAME, --name NAME  Sets the name of the output save file if the save flag
                        is True
*  -d DICTIONARY, --dictionary DICTIONARY
                        Sets the dictionary path
*  -e ENTROPY, --entropy ENTROPY
                        Sets the entropy flag. Set to True if you want to know
                        the entropy bits of the passwords
*  -E ENTROPYSAVE, --entropysave ENTROPYSAVE
                        Set to True if you want to save the entropy bits of
                        your passwords
