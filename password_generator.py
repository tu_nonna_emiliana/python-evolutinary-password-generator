import random
from random import shuffle
from math import floor
from math import log
import argparse


### control functions
def init(populationSize, words) :
    population = [[words[i]] for i in range(populationSize)]
    return population

def fitness(individual, words) :
    length = sum([len(i) for i in individual])
    if length > maxLength :
        return 0
    elif len(individual) > maxWordsPerPassword :
        return 0
    computeBits = lambda n,l : l * log(n,2) # computes entropy bits
    fitness = 50 * computeBits(len(words),len(individual)) + 10 * length
    return fitness

def evalEveryone(population,words) :
    # I must convert the list into a tuple since list aren't hashble
    return dict(zip([tuple(i) for i in population], [fitness(i,words) for i in population])) # this is dictonary comprised of a zip of every individual with its score

def select(populationScore, selectionPercentage) :
    theBest = [i[0] for i in sorted(populationScore.items(), key = lambda x : x[1], reverse = True)] # sort dictionary by keys
    return theBest[0 : floor(len(theBest)*selectionPercentage)]

### mutations
## unary mutations
def addWord(individual, words) :
    individual.insert(random.randint(0,len(individual)),words[random.randint(0,len(words)-1)])
    return individual

def removeWord(individual) :
    del individual[random.randint(0,len(individual)-1)]

## crossover mutations
def swap(individualA, individualB) :
    for i in range(min(len(individualA),len(individualB))) :
        if random.random() > 0.5 :
            individualA[i % len(individualA)], individualB[i % len(individualB)] = individualB[i % len(individualB)], individualA[i % len(individualA)]

def combine(individualA, individualB) :
    child = [[i,j] for i in individualA if random.random() > 0.5 for j in individualB if random.random() > 0.5]
    child = list(set([j for i in child for j in i])) # flatten and select unique elements
    return child

# the alter function alters the population using mutations
def alter(maxPopulation, population, words) :
    nPopulation = len(population)

    nAddWord, nRemoveWord, nSwap = random.randint(0,nPopulation), random.randint(0,nPopulation), random.randint(0,nPopulation) #number of mutations
    nChildren = maxPopulation - nPopulation # number of new children

    for i in range(nAddWord) :
        addWord(population[random.randint(0,nPopulation-1)],words)
    for i in range(nRemoveWord) :
        index = random.randint(0,nPopulation-1)
        if len(population[index]) != 0 :
            removeWord(population[index])
    for i in range(nSwap) :
        swap(population[random.randint(0,nPopulation-1)],population[random.randint(0,nPopulation-1)])
    for i in range(nChildren):
        population.append(combine(population[random.randint(0,nPopulation-1)],population[random.randint(0,nPopulation-1)]))
    shuffle(population)



### parse arguments
def parseArguments() :
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--words", help="Sets the maximum number of words of the password", type=int)
    parser.add_argument("-l", "--length", help="Sets the maximum length of the password ",type=int)
    parser.add_argument("-p","--population", help="Sets the population size",type=int)
    parser.add_argument("-g","--generations", help="Sets the number of generations. Feel free to trigger the keyboard interrupt during the execution if g is too high or the program slows down", type=int)
    parser.add_argument("-s","--seed", help="Sets the random seed. Useful for debugging or thinkering around.", type=int)
    parser.add_argument("--separator", help="Sets the word separator in the final output")
    parser.add_argument("--saveseparator", help="Set to True if you want to save the passwords with their separators", type=bool)
    parser.add_argument("--selection", help="Sets the selection percentage between one generations. Must be set between (0,1)", type=float)
    parser.add_argument("-f","--final", help="Sets the final selection percentage. Regulates the number of output Passwords. Must be set between (0,1)", type=float)
    parser.add_argument("-S", "--save", help="Sets the save flag. set to True if you want to save your passwords", type=bool)
    parser.add_argument("-n", "--name", help="Sets the name of the output save file if the save flag is True")
    parser.add_argument("-d", "--dictionary", help="Sets the dictionary path")
    parser.add_argument("-e", "--entropy", help="Sets the entropy flag. Set to True if you want to know the entropy bits of the passwords", type=bool)
    parser.add_argument("-E", "--entropysave", help="Set to True if you want to save the entropy bits of your passwords", type=bool)
    args = parser.parse_args()
    return args

def cleanInput(args) :

    if not args.words :
        args.words = 4
    if not args.length :
        args.length = 24
    if not args.population :
        args.population = 1024
    if not args.generations :
        args.generations = 32
    if args.seed == None:
        args.seed = None # lol, this line is useless
    if not args.separator :
        args.separator = '|'
    if not args.saveseparator :
        args.saveseparator = False
    if not args.selection :
        args.selection = 0.5
    if not args.final :
        args.final = 0.01
    if not args.save :
        args.save = False
    if not args.name :
        args.name = "passwords"
    if not args.dictionary :
        args.dictionary = "words_alpha.txt"
    if not args.entropy :
        args.entropy = False
    if not args.entropysave :
        args.entropysave = False
    return args


# arguments
args = parseArguments()
args = cleanInput(args)

maxWordsPerPassword, maxLength, populationSize, maxGenerations, seed, wordSeparator, saveSeparator, selectionPercentage, finalSelectionPercentage, save, name, dictionary, entropy, entropySave = args.words, args.length, args.population, args.generations, args.seed, args.separator, args.saveseparator, args.selection, args.final, args.save, args.name, args.dictionary, args.entropy, args.entropysave

words = open(args.dictionary)
words = [i.strip() for i in words.readlines()]

random.seed(seed)
shuffle(words)

population = init(populationSize, words) # list of list of strings; every list of strings is a password
populationScore = evalEveryone(population, words) # evaluates the population. populationScore is a dictionary

for i in range(maxGenerations):
    try :
        print("generation number [" + str(i) + "]")
        population = select(populationScore, selectionPercentage) # selects the best of the population
        population = [list(i) for i in population] # convert tuples to array
        alter(populationSize, population, words)
        populationScore = evalEveryone(population,words)
    except KeyboardInterrupt :
        break

population = select(populationScore, finalSelectionPercentage)
passwords = ''

computeBits = lambda n,l : l * log(n,2) # computes entropy bits

for individual, index in zip(population,range(len(population))):
    print("The password number " + str(index) + " is : |",end="")
    if saveSeparator :
        passwords+=wordSeparator
    for i in individual :
        passwords+=i
        if saveSeparator :
            passwords+=wordSeparator
        print(i,end=wordSeparator)
    if entropy :
        print(' = ' + str(round(computeBits(len(words), len(individual)),2)) + " bits", end="")
    if entropySave :
        passwords += ' = ' + str(computeBits(len(words), len(individual)))
    passwords += '\n'
    print()

if save :
    file = open(name+'.txt', 'w')
    file.write(passwords)
    file.close()
